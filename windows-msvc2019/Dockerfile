#escape=`
FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2022

LABEL Description="Windows Base for use with Craft"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

ENV chocolateyUseWindowsCompression=false

# Visual Studio is a bit delicate, ensure it is running under cmd.exe for maximum compatibility
SHELL ["cmd.exe", "/S", "/C"]

# Based on https://docs.microsoft.com/en-us/visualstudio/install/build-tools-container
# Install Build Tools with the vc++ workload, excluding workloads and components with known issues.
RUN `
    # Download the latest Build Tools bootstrapper
    # You cannot use the correct version for VS 2019 as that will fail after installing very little with no errors
    # This likely has something to do with it installing the latest VS installer
    curl -SL --output vs_buildtools.exe https://aka.ms/vs/17/release/vs_buildtools.exe  `
    `
    # Install Visual Studio with the components we need, excluding workloads and components with known issues.
    # We instruct it to not update the installer as that just causes too many issues
    # We have to use channeluri/installchanneluri as we are abusing the MSVC 2022 installer to bring in 2019 (see above notes)
    && (start /w vs_buildtools.exe --quiet --wait --norestart --nocache --noUpdateInstaller `
        --channeluri https://aka.ms/vs/16/release/channel `
        --installchanneluri https://aka.ms/vs/16/release/channel `
        --add Microsoft.VisualStudio.Component.VC.ATL `
        --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 `
        --add Microsoft.VisualStudio.Component.VC.CoreBuildTools `
        --add Microsoft.VisualStudio.Component.VC.CLI.Support `
        --add Microsoft.VisualStudio.Component.Windows10SDK `
        --add Microsoft.VisualStudio.Component.Windows10SDK.19041 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.10240 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.10586 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.14393 `
        --remove Microsoft.VisualStudio.Component.Windows81SDK `
        || IF "%ERRORLEVEL%"=="3010" EXIT 0) `
    `
    # Cleanup
    && del /q vs_buildtools.exe `
    && powershell -Command "Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose"

# Switch to Powershell from here on now that Visual Studio is all happy
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# Install chocolatey, then use it to install Git, Python 2 and 3, 7zip and Powershell Core
RUN iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); `
    choco install -y git python2 python3 7zip powershell-core; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

# Make sure Git does not attempt to use symlinks as they don't work well with CMake and co
RUN git config --system core.symlinks false
